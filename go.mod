module cakrome/gohnt

go 1.18

require (
	github.com/alexflint/go-scalar v1.2.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require github.com/alexflint/go-arg v1.5.1
