GOBUILD=CGO_ENABLED=0 go build -ldflags '-w -s -buildid=' -trimpath
BINDIR=build
BINARY=gohnt

.PHONY: all linux-x86_64 linux-aarch64 linux-ppc64le linux-s390x linux-riscv64 release clean

all: linux-x86_64 linux-aarch64 linux-ppc64le linux-s390x linux-riscv64

linux-x86_64:
	GOARCH=amd64 GOOS=linux $(GOBUILD) -o $(BINDIR)/$(BINARY)-$@/$(BINARY) gohnt.go

linux-aarch64:
	GOARCH=arm64 GOOS=linux $(GOBUILD) -o $(BINDIR)/$(BINARY)-$@/$(BINARY) gohnt.go

linux-ppc64le:
	GOARCH=ppc64le GOOS=linux $(GOBUILD) -o $(BINDIR)/$(BINARY)-$@/$(BINARY) gohnt.go

linux-s390x:
	GOARCH=s390x GOOS=linux $(GOBUILD) -o $(BINDIR)/$(BINARY)-$@/$(BINARY) gohnt.go

linux-riscv64:
	GOARCH=riscv64 GOOS=linux $(GOBUILD) -o $(BINDIR)/$(BINARY)-$@/$(BINARY) gohnt.go

release: linux-x86_64 linux-aarch64 linux-ppc64le linux-s390x linux-riscv64
	$(foreach t,$^,tar -czf $(BINDIR)/$(BINARY)-$(t).tar.gz -C $(BINDIR) gohnt-$(t);)

clean:
	rm -rf $(BINDIR)
