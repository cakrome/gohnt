package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"cakrome/gohnt/libgohnt"

	"github.com/alexflint/go-arg"
)

func main() {
	gohnt_version := "0.2.1"

	var sss_list []string
	var num_story_to_fetch int

	var args struct {
		Num     int  `arg:"-N,--num" help:"number of stories to fetch" default:"20"`
		Version bool `arg:"-V,--version" help:"show the version of the program"`
	}
	arg.MustParse(&args)

	if args.Version {
		fmt.Printf("gohnt %s\n", gohnt_version)
		os.Exit(0)
	}

	num_story_to_fetch = args.Num
	if args.Num <= 0 {
		fmt.Println("Number of stories to fetch cannot be less or equal to 0.")
		os.Exit(1)
	} else if args.Num > 500 {
		num_story_to_fetch = 500
		str_to_print := fmt.Sprintf("Max is %d stories.\n", num_story_to_fetch)
		fmt.Println(str_to_print)
	}

	sss_list = libgohnt.Story_list_gen(num_story_to_fetch)
	// Remove the last new line from the last story
	sss_list[len(sss_list)-1] = strings.TrimSuffix(sss_list[len(sss_list)-1], "\n")
	for index, items := range sss_list {
		fmt.Println(strconv.Itoa(index+1) + ". " + items)
	}
}
