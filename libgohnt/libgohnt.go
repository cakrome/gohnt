package libgohnt

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strconv"
	"sync"
)

const API_URL string = "https://hacker-news.firebaseio.com/v0"

type story struct {
	ID           int    `json:"id"`
	Title        string `json:"title"`
	URL          string `json:"url"`
	Score        int    `json:"score"`
	By           string `json:"by"`
	Comments_num int    `json:"descendants"`
}

func get_story_list() []int {
	resp_ll, getErr := http.Get(API_URL + "/topstories.json")
	if getErr != nil {
		log.Fatal(getErr)
	}
	body, readErr := io.ReadAll(resp_ll.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}
	var story_list []int

	Eeeerr := json.Unmarshal(body, &story_list)
	if Eeeerr != nil {
		log.Fatal(Eeeerr)
	}
	return story_list
}

func story_generator(story_id int) string {
	resp, getErr := http.Get(API_URL + "/item/" + strconv.Itoa(story_id) + ".json")
	if getErr != nil {
		log.Fatal(getErr)
	}
	body, readErr := io.ReadAll(resp.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}
	actual_story := story{}
	jsonErr := json.Unmarshal(body, &actual_story)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}
	up := actual_story.Title + "\n" + "Score: " + strconv.Itoa(actual_story.Score) + "   Comments: " + strconv.Itoa(actual_story.Comments_num) + "   User: " + actual_story.By
	url_direct := "Direct URL: " + actual_story.URL
	url := "Hacker News URL: https://news.ycombinator.com/item?id=" + strconv.Itoa(actual_story.ID)
	str_story := up + "\n" + url + "\n" + url_direct + "\n"
	return str_story
}

func Story_list_gen(num_of_stories int) []string {
	story_index_list := get_story_list()
	var story_ll []string
	var wg sync.WaitGroup
	var mu sync.Mutex
	wg.Add(num_of_stories)

	for ind := 0; ind < num_of_stories; ind++ {
		go func(ind int) {
			defer wg.Done()
			sssl := story_generator(story_index_list[ind])
			mu.Lock()
			story_ll = append(story_ll, sssl)
			mu.Unlock()
		}(ind)
	}
	wg.Wait()
	return story_ll
}
